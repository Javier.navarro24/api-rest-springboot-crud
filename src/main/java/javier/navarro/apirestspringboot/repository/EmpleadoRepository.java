package javier.navarro.apirestspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javier.navarro.apirestspringboot.model.Employee;

@Repository
public interface EmpleadoRepository extends JpaRepository<Employee, Long> {

	
}
