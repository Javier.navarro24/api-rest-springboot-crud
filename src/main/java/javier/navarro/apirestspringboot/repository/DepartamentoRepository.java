package javier.navarro.apirestspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javier.navarro.apirestspringboot.model.Department;

@Repository
public interface DepartamentoRepository extends JpaRepository<Department, Long> {

}
