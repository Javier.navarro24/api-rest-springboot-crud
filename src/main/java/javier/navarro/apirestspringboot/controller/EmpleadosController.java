package javier.navarro.apirestspringboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import javier.navarro.apirestspringboot.api.ApiV1;
import javier.navarro.apirestspringboot.model.Employee;
import javier.navarro.apirestspringboot.model.Job;
import javier.navarro.apirestspringboot.service.EmpleadoService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/empleados")
@Tag(name = "Controller Empleados",description = "Metodos de empleados")
public class EmpleadosController extends ApiV1 {

	@Autowired
	private EmpleadoService servicioEmp;

	@GetMapping("/lista")
	public List<Employee> obtenerEmpleado() {
		return servicioEmp.obtenerEmpleados();
	}


	@GetMapping("/empleado/{id}")

	public ResponseEntity<Employee> obtenerEmpleadoId (@PathVariable Long id){

		Optional<Employee> empleado = servicioEmp.getEmployeeById(id);

		if(empleado.isPresent()) {

			return new ResponseEntity<>(empleado.get(), HttpStatus.OK);

		}else {

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}



	@PostMapping("/nuevo")
	public boolean nuevoEmpleado(@RequestBody Employee empleado) {

		Job j =  new Job(); //al ignorar la relacion para swagger lo iniciamos aqui.
		j.setJobId("");
	
		
		if(empleado != null){
			
		
			empleado.setJob(j);
			empleado.getJob().setJobId(empleado.getJobId());

			servicioEmp.createEmployees(empleado);
			
			return true;
		}

		return false;
	}


	@DeleteMapping("/eliminar/{id}")
	public boolean eliminarEmpleado(Long id) {

		if(id != null){

			servicioEmp.deleteEmployee(id);
			
			return true;
		}

		return false;
	}
}



