package javier.navarro.apirestspringboot.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javier.navarro.apirestspringboot.model.Department;
import javier.navarro.apirestspringboot.service.DepartamentoService;
/*
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/departamentos")
public class DepartamentoController {
	
	@Autowired
	private DepartamentoService servicioDep;


	
	@GetMapping("/lista")
	public List<Department> obtenerDepart() {

		return servicioDep.obtenerDepartamentos();
	}
	
	@GetMapping("/departamento/{id}")
	
	public ResponseEntity<?> obtenerDepartamentoId (@PathVariable Long id){

		Optional<Department> departamento = servicioDep.getDepartmentById(id);

		if(departamento.isPresent()) {
			
			return new ResponseEntity<>(departamento, HttpStatus.OK);
		
		}else {

		return new ResponseEntity<>("departamento no encontrado!.", HttpStatus.NOT_FOUND);
	}
}
	
	@PostMapping("/nuevo")
	public void nuevoDepartamento(@RequestBody Department departamento) {
		servicioDep.createDepartments(departamento);
	}

	
	@DeleteMapping("/eliminar/{id}")
	@CrossOrigin
	public void eliminarDepartamento(@PathVariable Long id, HttpServletResponse response) {
		servicioDep.deleteDepart(id);
	}
	
}
*/
