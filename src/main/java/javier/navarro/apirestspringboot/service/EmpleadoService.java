package javier.navarro.apirestspringboot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javier.navarro.apirestspringboot.model.Employee;
import javier.navarro.apirestspringboot.repository.EmpleadoRepository;

@Service
public class EmpleadoService {

	@Autowired
	private EmpleadoRepository repositorioEmp;
	
	//Obtener todos los empleados
	public List<Employee> obtenerEmpleados() {
		
		return repositorioEmp.findAll();
	}
	
	
	//Obtener empleado por ID
	public Optional<Employee> getEmployeeById(Long id) {
		
		return repositorioEmp.findById(id);
		
	}
	
	
	//Crear empleado
	public void createEmployees(Employee employee) {
		repositorioEmp.save(employee);

	}
	
	
	
	//Borrar empleado
	public void deleteEmployee(Long id) {
		repositorioEmp.deleteById(id);
	}
	
}
