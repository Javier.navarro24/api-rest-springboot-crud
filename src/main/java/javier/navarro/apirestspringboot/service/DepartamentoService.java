package javier.navarro.apirestspringboot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javier.navarro.apirestspringboot.model.Department;
import javier.navarro.apirestspringboot.repository.DepartamentoRepository;

@Service
public class DepartamentoService {

	@Autowired
	private DepartamentoRepository repositorioDep;
	
	public List<Department> obtenerDepartamentos() {
		List<Department> departments = new ArrayList<Department>();
		repositorioDep.findAll().forEach(departments::add);
		return departments;
	}
	
	//Obtener empleado por ID
	public Optional<Department> getDepartmentById(Long id) {
		Optional<Department> resultado = repositorioDep.findById(id);
		
		if (resultado.isPresent()) {
			resultado.get();
		}
		return resultado;
		
	}
	
	/*
	public void deleteDepart(Long department_id) {
		repositorioDep.delete(new Department ());

	}
	*/

	public void createDepartments(Department department) {
		repositorioDep.save(department);

	}
}
